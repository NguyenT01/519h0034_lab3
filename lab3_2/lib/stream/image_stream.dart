import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class RefactorStream {


  Stream<String> getText() async* {
    Future<List<String>> ftList = fetchImage();
    List<String> messes = await ftList;

    yield* Stream.periodic(Duration(seconds: 5), (int i) {
      int index = i% messes.length;
      print(messes[index]);
      return messes[index];
    });
  }


  //Tạm thời em lấy link URL của Lab1 cũ, do url mới bị lỗi không hiển thị ảnh được
  Future<List<String>> fetchImage() async{
    List<String> img = [];
    var urlComponent = "https://thachln.github.io/toeic-data/ets/2016/1/p1/";
    //var url = Uri.parse('https://jsonplaceholder.typicode.com/photos');

    var url = Uri.parse('https://thachln.github.io/toeic-data/ets/2016/1/p1/data.json');
    var response = await http.get(url);
    var jsonData = response.body;

    //print(jsonData);

    var jsonObject = jsonDecode(jsonData);

    for(int i=0; i<jsonObject.length;i++){
      img.add(
          urlComponent+jsonObject[i]['image']
      );
    }
    //print(img);
    return img;
  }
}
