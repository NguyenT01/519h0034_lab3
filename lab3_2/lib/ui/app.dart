import 'package:flutter/material.dart';
import '../stream/image_stream.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login me',
      home: Scaffold(
        appBar: AppBar(
          title: Text("Lab3 ex2"),
        ),
        body: StreamLayout(),
      ),
    );
  }
}

class StreamLayout extends StatefulWidget {
  const StreamLayout({Key? key}) : super(key: key);

  @override
  State<StreamLayout> createState() => _StreamLayoutState();
}

class _StreamLayoutState extends State<StreamLayout> {
  List<String> mess= [];
  RefactorStream ts = RefactorStream();
  
  @override
  void initState() {
    // TODO: implement initState
    ts.fetchImage();
    addText();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.all(16),
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: mess.length,
          itemBuilder: (BuildContext context,int index){
            return Card(
              child: Container(
                child: Image.network(mess[index], width: 200,height: 200,),
              ),
              elevation: 10,
            );
          },
        ),
      ),
    );
  }

  addText() async{
    ts.getText().listen((event) {
      setState(() {
        mess.add(event);
      });
    });
  }


}
