import 'dart:async';

class TextStream {
  Stream<String> getText() async* {
    final List<String> messes = ["Hello", "World", "We", "Are", "Together!"];

    yield* Stream.periodic(Duration(seconds: 5), (int i) {
      int index = i % 5;
      return messes[index];
    });
  }
}
